<html>
<head>
	<title> Confirmation</title>
</head>

<body>

Full name : ${param.firstName} ${param.lastName} 
<br/>
Favorite Language : ${param.favouriteLanguage }
<br/>
Favorite Food : 

<% 
	String[] foods = request.getParameterValues("favouriteFood");
	if(foods != null){
		for (String s:foods)
			out.println("<li> " + s + "</li>");
	}
%>
</body>

</html>