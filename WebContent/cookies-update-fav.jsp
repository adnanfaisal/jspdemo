<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cookies updated</title>
</head>
<body>
<% 
	String favLang = request.getParameter("language");
	Cookie cookie = new Cookie("favLang",favLang);
	cookie.setMaxAge(60*60*24);
	response.addCookie(cookie);
%>
	Your favorite language has been set as <%=  favLang %>
	<br>
	<a href='cookies-home.jsp'> Go back to home page </a>



</body>
</html>