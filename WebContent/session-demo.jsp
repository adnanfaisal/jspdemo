<%@ page import="java.util.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles.css">
<title>Insert title here</title>
</head>
<body>
Add a thing to do :

<form action="session-demo.jsp">
<p>
<input type="text" name="aToDo"> 
<input type="submit" value="Submit"> 
</p>
</form>

<% 
	String todo = request.getParameter("aToDo");
	List<String> todos = (List<String>)  session.getAttribute("todos");
	if(todos == null){
		 todos = new ArrayList<>();		 
	}
	if(todo != null && !todo.isEmpty()) todos.add(todo);
	session.setAttribute("todos", todos);
	out.println("<br> <ul>");
		for(String t:todos)
			out.println("<li>" + t);
	out.println("</ul>");
%>
</body>
</html>