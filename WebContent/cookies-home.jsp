<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% String favLang = "java"; 
   
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(Cookie cookie: cookies){
			if(cookie.getName().equals("favLang")){
				favLang = cookie.getValue();
			}
		}
	}
%>

  <h1>Your Favorite Programming Language is : <%=   favLang %> </h1> <br>

   <h2>Some news about <%=   favLang %> </h2> <br>
   blah blah blah<br>
   blah blah blah<br>
   

   <h2>Some blog post about <%=   favLang %> </h2> <br>
   blah blah blah<br>
   blah blah blah<br>

  

   <a href="cookies-choose-fav.jsp"> Update favorite language</a>

</body>
</html>